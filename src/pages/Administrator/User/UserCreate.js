import React, { Component } from 'react'
import axios from 'axios'

class UserCreate extends Component {
  state = {
    firstName: '',
    lastName: '',
  }

  handleChangeFirstName = e => this.setState({ firstName: e.target.value })
  
  handleChangeLastName = e => this.setState({ lastName: e.target.value })

  handleSubmit = e => {
    e.preventDefault()

    const newUser = {
      firstName: this.state.firstName,
      lastName: this.state.lastName
    }

    axios.post('http://localhost:9000/api/user', newUser)
      .then(res => {
        this.props.history.push('/')
      })
      .catch(err => {
        console.log(err)
      })
  }

  render() {
    return (
      <div>
        <h1>Create</h1>
        <form onSubmit={this.handleSubmit}>
          <div>
            <label>First name</label>
            <input type="text" name="firstName" value={this.state.firstName} onChange={this.handleChangeFirstName} />
          </div>
          <div>
            <label>Last name</label>
            <input type="text" name="lastName" value={this.state.lastName} onChange={this.handleChangeLastName} />
          </div>
          <input type="checkbox" name="gender[]"value="male"/>
          <div>
            <select name="gago" multiple>
              <option></option>
              <option value="1">1</option>
              <option value="2">2</option>
            </select>
          </div>
          <input type="checkbox" name="gender[]"value="female"/>
          <input type="submit" value="Submit" />
        </form>
      </div>
    )
  }
}

export default UserCreate