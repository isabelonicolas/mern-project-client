import React, { Component } from 'react'
import axios from 'axios'

class UserEdit extends Component {
  _isMounted = false

  state = {
    firstName: '',
    lastName: '',
  }

  componentDidMount() {
    this.loadUser()
  }

  loadUser = () => {
    axios.get(`http://localhost:9000/api/user/${this.props.match.params.id}`)
    .then(res => {
      this.setState({
        firstName: res.data.firstName,
        lastName: res.data.lastName
      })
    })
    .catch(err =>{
      console.log(err)
    })
  }

  handleChangeFirstName = e => this.setState({ firstName: e.target.value })
  handleChangeLastName = e => this.setState({ lastName: e.target.value })

  handleSubmit = e => {
    e.preventDefault()

    const updatedData = {
      firstName: this.state.firstName,
      lastName: this.state.lastName
    }

    axios.put(`http://localhost:9000/api/user/${this.props.match.params.id}`, updatedData)
      .then(res => {
        this.props.history.push('/')
      })
      .catch(err => {
        console.log(err)
      })
  }

  render() {
    return (
      <div className="container">
        <div>
          <h1>TODO | Edit</h1>
        </div>
        <form onSubmit={this.handleSubmit}>
          <div>
            <label>First name</label>
            <input type="text" value={this.state.firstName} onChange={this.handleChangeFirstName} />
          </div>
          <div>
            <label>Last name</label>
            <input type="text" value={this.state.lastName} onChange={this.handleChangeLastName} />
          </div>
          <input type="submit" value="Submit" />
        </form>
      </div>
    )
  }
}

export default UserEdit