import styled from 'styled-components'
import MediaHandler from '../../../components/uielements/mediahandler'
import { Text } from '../../../components/uielements/typography'


export const TableCellContent = styled.div`
  display: flex;
`
export const TableCellMediaHandlerWrapper = styled.div`
  flex: 0 0 auto;
  margin-right: 10px;
  width: 50px;
`
export const TableCellMediaHandler = styled(MediaHandler)`
  flex: none;
`
export const TableCellText = styled(Text)`
  flex: none;
`

