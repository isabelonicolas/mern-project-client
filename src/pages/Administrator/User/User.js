import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'
import swal from 'sweetalert2'

import { Container } from '../../../components/container'
import { Table, TableHead, TableBody, TableRow, TableCell } from '../../../components/uielements/table'
import { Button } from '../../../components/uielements/button'
import { IconAvatarBoy } from '../../../components/icons/'
import { Heading, Paragraph, Text } from '../../../components/uielements/typography'

import { TableCellContent, TableCellMediaHandlerWrapper, TableCellMediaHandler, TableCellText } from './index.style'

class User extends Component {
  state = {
    users: [],
    tableLoading: false
  }

  componentDidMount() {
    this.loadUsers()
  }

  loadUsers = () => {
    this.setState({
      tableLoading: true
    })

    axios.get('http://localhost:9000/api/user')
      .then(res => {
        this.setState({
          users: res.data,
          tableLoading: false
        })
      })
      .catch(err => {
        console.log(err)
      })
  }

  openUserProfile = () => {
    // swal.fire('[View Profile]')
  }

  handleDeleteUser = (id, event) => {
    event.stopPropagation();

    swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      showCancelButton: true,
      showLoaderOnConfirm: true,
      preConfirm: () => {
        return axios.delete(`http://localhost:9000/api/user/${id}`)
          .then(res => {
            return res.data
          })
          .catch(err => {
            console.log(err)
          })
      },
      allowOutsideClick: () => !swal.isLoading(),
    })
    .then(res => {
      if( res.value ) {
        swal.fire(
          'Deleted',
          res.value
        )
        this.loadUsers()
      }
    })
  }

  handleRowClick = (event) => {
    event.stopPropagation();

    this.openUserProfile()
  }

  handleRowKeyDown = (event) => {
    // // event.stopPropagation();
    
    // if ( event.keyCode === 13 ) {
    //   this.openUserProfile()
    // }
  }

  render() {
    return(
      <Container>
        {/* HEADING 1
        <Heading is="h1" size="title1">Lorem Ipsum</Heading>
        <Heading is="h1" size="title2">Lorem Ipsum</Heading>
        <Heading is="h1" size="title3">Lorem Ipsum</Heading>
        <Heading is="h1" size="large">Lorem Ipsum</Heading>
        <Heading is="h1" size="regular">Lorem Ipsum</Heading>
        <Heading is="h1" size="small">Lorem Ipsum</Heading>
        <Heading is="h1" size="micro">Lorem Ipsum</Heading>
        <hr/>
        HEADING 2
        <Heading size="title1">Lorem Ipsum</Heading>
        <Heading size="title2">Lorem Ipsum</Heading>
        <Heading size="title3">Lorem Ipsum</Heading>
        <Heading size="large">Lorem Ipsum</Heading>
        <Heading size="regular">Lorem Ipsum</Heading>
        <Heading size="small">Lorem Ipsum</Heading>
        <Heading size="micro">Lorem Ipsum</Heading>
        <hr/>
        HEADING 3
        <Heading is="h3" size="title1">Lorem Ipsum</Heading>
        <Heading is="h3" size="title2">Lorem Ipsum</Heading>
        <Heading is="h3" size="title3">Lorem Ipsum</Heading>
        <Heading is="h3" size="large">Lorem Ipsum</Heading>
        <Heading is="h3" size="regular">Lorem Ipsum</Heading>
        <Heading is="h3" size="small">Lorem Ipsum</Heading>
        <Heading is="h3" size="micro">Lorem Ipsum</Heading>
        <hr/>
        HEADING 4
        <Heading is="h4" size="title1">Lorem Ipsum</Heading>
        <Heading is="h4" size="title2">Lorem Ipsum</Heading>
        <Heading is="h4" size="title3">Lorem Ipsum</Heading>
        <Heading is="h4" size="large">Lorem Ipsum</Heading>
        <Heading is="h4" size="regular">Lorem Ipsum</Heading>
        <Heading is="h4" size="small">Lorem Ipsum</Heading>
        <Heading is="h4" size="micro">Lorem Ipsum</Heading>
        <hr/>
        HEADING 5
        <Heading is="h5" size="title1">Lorem Ipsum</Heading>
        <Heading is="h5" size="title2">Lorem Ipsum</Heading>
        <Heading is="h5" size="title3">Lorem Ipsum</Heading>
        <Heading is="h5" size="large">Lorem Ipsum</Heading>
        <Heading is="h5" size="regular">Lorem Ipsum</Heading>
        <Heading is="h5" size="small">Lorem Ipsum</Heading>
        <Heading is="h5" size="micro">Lorem Ipsum</Heading>
        <hr/>
        HEADING 6
        <Heading is="h6" size="title1">Lorem Ipsum</Heading>
        <Heading is="h6" size="title2">Lorem Ipsum</Heading>
        <Heading is="h6" size="title3">Lorem Ipsum</Heading>
        <Heading is="h6" size="large">Lorem Ipsum</Heading>
        <Heading is="h6" size="regular">Lorem Ipsum</Heading>
        <Heading is="h6" size="small">Lorem Ipsum</Heading>
        <Heading is="h6" size="micro">Lorem Ipsum</Heading>
        <hr/>
        PARAGRAPH
        <Paragraph>Lorem Ipsum</Paragraph>
        <hr/>
        TEXT<br/>
        <Text>Lorem Ipsum</Text> */}
        

        <Table name="Users" loading={this.state.tableLoading}>
          <TableHead>
            <TableRow>
              <TableCell title="Name" width="300">Name</TableCell>
              <TableCell title="Verified">Verified</TableCell>
              <TableCell title="Action">Action</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this.state.users.length > 0 ?
              this.state.users.map((data, i) => (
                <TableRow
                  key={data._id}
                  enableFocus
                  handleKeyDown={this.handleRowKeyDown.bind(this)}
                  handleClick={this.handleRowClick.bind(this)}>
                  <TableCell title={`${data.firstName} ${data.lastName}`}>
                    <TableCellContent className="TableCellCOntent">
                      <TableCellMediaHandlerWrapper>
                        <TableCellMediaHandler>
                          <IconAvatarBoy />
                        </TableCellMediaHandler>
                      </TableCellMediaHandlerWrapper>
                      <TableCellText>
                        {`${data.firstName} ${data.lastName}`}<br/>
                        User
                      </TableCellText>
                    </TableCellContent>
                  </TableCell>
                  <TableCell>Yes</TableCell>
                  <TableCell>
                    <Link to={`/edit/${data._id}`} onClick={event => event.stopPropagation()}>
                      Edit
                    </Link>
                    <Button handleClick={this.handleDeleteUser.bind(this, data._id)}>
                      Delete
                    </Button>
                  </TableCell>
                </TableRow>
              )) : 
              <TableRow>
                <TableCell>No data found.</TableCell>
              </TableRow>
            }
          </TableBody>
        </Table>
      </Container>
    )
  }
}

export default User