import IconAvatarBoy from './avatar-boy'
import IconAvatarGirl from './avatar-girl'
import IconLoading from './loading'

export {IconAvatarBoy, IconAvatarGirl, IconLoading} 