import styled from 'styled-components'

const Table = styled.table`
  border-collapse: collapse;
  border-radius: 5px;
  box-sizing: border-box;
  font-size: 14px;
  min-height: 50vh;
  position: relative;
  width: 100%;
`
const TableRow = styled.tr`
  // display: flex;
  // flex-wrap: wrap;
  // justify-content: space-evenly

  @media (min-width: 576px) {
    // flex-wrap: nowrap;
  }
`
const TableCell = styled.td`
  // flex: 1 1 100%;
  padding: 15px;
  
  text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;

  @media (min-width: 576px) {
    // flex: 1 1 100%;
    
  }
`
const TableHead = styled.thead`
  // display: none;

  ${TableRow} {
    font-weight: bold;
    margin-bottom: 15px;
    text-transform: uppercase;
  }

  ${TableCell} {
    padding: 0 15px;
  }

  @media (min-width: 576px) {
    // display: block;
  }
`
const TableBody = styled.tbody`
  ${TableRow} {
    background-color: #ffffff;
    border-radius: 5px;
    cursor: pointer;

    &:nth-child(even) {
      background-color: #fafafa;
    }
    
    &:hover,
    &:focus,
    &:focus-within {
      background-color: #e2f5e5;
      outline: 0;
    }
  }
`
const TableLoader = styled.div`
  background-color: rgba(255,255,255,0.75);
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100%;
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 10;
  width: 100%;

  > svg {
    height: 100px;
    width: 100px;
  }
`
export { Table, TableHead, TableBody, TableRow, TableCell, TableLoader }