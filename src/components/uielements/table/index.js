import React from 'react';

import { IconLoading } from '../../icons'
import { 
  Table as SCTable,
  TableHead as SCTableHead,
  TableBody as SCTableBody,
  TableRow as SCTableRow,
  TableCell as SCTableCell,
  TableLoader as SCTableLoader
} from './table.style'

const Table = props => {
  return(
    <SCTable>
      {props.children}
      { props.loading &&
        <SCTableLoader>
          <IconLoading />
        </SCTableLoader>
      }
    </SCTable>
  );
}

const TableHead = props => {
  return(
    <SCTableHead>
      {props.children}
    </SCTableHead>
  );
}

const TableBody = props => {
  return(
    <SCTableBody>
      {props.children}
    </SCTableBody>
  );
}

const TableRow = props => {
  const tableRowProperties = props.enableFocus && {"tabIndex": 0, "role": "button"}

  return(
    <SCTableRow {...tableRowProperties} onClick={props.handleClick} onKeyDown={props.handleKeyDown} role="button">
      {props.children}
    </SCTableRow>
  )
}

const TableCell = props => {
  return(
    <SCTableCell title={props.title} className={props.title} width={props.width}>
      {props.children}
    </SCTableCell>
  )
}

export {Table, TableHead, TableBody, TableRow, TableCell};
