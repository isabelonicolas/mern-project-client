import React from 'react'
import {MediaHandler as SCMediaHandler} from './mediahandler.style'

const MediaHandler = props => (
  <SCMediaHandler className={props.className}>
    {props.children}
  </SCMediaHandler>
)

export default MediaHandler