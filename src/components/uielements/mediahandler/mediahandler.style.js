import styled from 'styled-components'

const MediaHandler = styled.div`
  position: relative;
  padding-top: 100%;

  > svg {
    height: 100%;
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    width: 100%;
  }

  > img {
    object-fit: cover;
    object-position: center;
    height: 100%;
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    width: 100%;
  }



`

export { MediaHandler }