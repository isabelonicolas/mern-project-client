import React from 'react'
import { SCButton } from './button.style'

export const Button = props => (
  <SCButton onClick={props.handleClick}>{props.children}</SCButton>
)