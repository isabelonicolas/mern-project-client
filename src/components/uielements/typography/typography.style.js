import styled, {css} from 'styled-components'

const title1 = `
  font-size: 44px;
  font-weight: 700;
  line-height: 1.5em;
  margin: 0 0 0 0;
`
const title2 = `
  font-size: 32px;
  font-weight: 700;
  line-height: 1.5em;
  margin: 0 0 0 0;
`
const title3 = `
  font-size: 24px;
  font-weight: 400;
  line-height: 1.5em;
  margin: 0 0 0 0;
`
const large = `
  font-size: 20px;
  font-weight: 400;
  line-height: 1.5em;
  margin: 0 0 0 0;
`
const regular = `
  font-size: 15px;
  font-weight: 400;
  line-height: 1.5em;
  margin: 0 0 0 0;
`
const small = `
  font-size: 12px;
  font-weight: 400;
  line-height: 1.5em;
  margin: 0 0 0 0;
`
const micro = `
  font-size: 10px;
  font-weight: 500;
  line-height: 1.5em;
  margin: 0 0 0 0;
`

export const StyledParagraph = styled.p`
  ${ css`${regular}` }
`

export const StyledText = styled.span`
  ${ css`${regular}` }
`

export const StyledH1 = styled.h1`
  ${ props => {
    return props.size === 'title1' ?
      css`${title1}` :
    props.size === 'title2' ?
      css`${title2}` :
    props.size === 'title3' ?
      css`${title3}` :
    props.size === 'large' ?
      css`${large}` :
    props.size === 'regular' ?
      css`${regular}` :
    props.size === 'small' ?
      css`${small}` :
    props.size === 'micro' ?
      css`${micro}` :  css`${regular}`
    }
  }
`

export const StyledH2 = styled.h2`
  ${ props => {
    return props.size === 'title1' ?
      css`${title1}` :
    props.size === 'title2' ?
      css`${title2}` :
    props.size === 'title3' ?
      css`${title3}` :
    props.size === 'large' ?
      css`${large}` :
    props.size === 'regular' ?
      css`${regular}` :
    props.size === 'small' ?
      css`${small}` :
    props.size === 'micro' ?
      css`${micro}` :  css`${regular}`
    }
  }
`

export const StyledH3 = styled.h3`
  ${ props => {
    return props.size === 'title1' ?
      css`${title1}` :
    props.size === 'title2' ?
      css`${title2}` :
    props.size === 'title3' ?
      css`${title3}` :
    props.size === 'large' ?
      css`${large}` :
    props.size === 'regular' ?
      css`${regular}` :
    props.size === 'small' ?
      css`${small}` :
    props.size === 'micro' ?
      css`${micro}` :  css`${regular}`
    }
  }
`

export const StyledH4 = styled.h4`
  ${ props => {
    return props.size === 'title1' ?
      css`${title1}` :
    props.size === 'title2' ?
      css`${title2}` :
    props.size === 'title3' ?
      css`${title3}` :
    props.size === 'large' ?
      css`${large}` :
    props.size === 'regular' ?
      css`${regular}` :
    props.size === 'small' ?
      css`${small}` :
    props.size === 'micro' ?
      css`${micro}` :  css`${regular}`
    }
  }
`

export const StyledH5 = styled.h5`
  ${ props => {
    return props.size === 'title1' ?
      css`${title1}` :
    props.size === 'title2' ?
      css`${title2}` :
    props.size === 'title3' ?
      css`${title3}` :
    props.size === 'large' ?
      css`${large}` :
    props.size === 'regular' ?
      css`${regular}` :
    props.size === 'small' ?
      css`${small}` :
    props.size === 'micro' ?
      css`${micro}` :  css`${regular}`
    }
  }
`

export const StyledH6 = styled.h6`
  ${ props => {
    return props.size === 'title1' ?
      css`${title1}` :
    props.size === 'title2' ?
      css`${title2}` :
    props.size === 'title3' ?
      css`${title3}` :
    props.size === 'large' ?
      css`${large}` :
    props.size === 'regular' ?
      css`${regular}` :
    props.size === 'small' ?
      css`${small}` :
    props.size === 'micro' ?
      css`${micro}` :  css`${regular}`
    }
  }
`