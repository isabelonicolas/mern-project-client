import React from 'react';

import { StyledParagraph, StyledText, StyledH1, StyledH2, StyledH3, StyledH4, StyledH5, StyledH6 } from './typography.style';

export const Heading = props => {
  return(
    <React.Fragment>
      {
        props.is === 'h1' ? 
          <StyledH1 size={props.size}>{props.children}</StyledH1> :
        props.is === 'h3' ?
          <StyledH3 size={props.size}>{props.children}</StyledH3> :
        props.is === 'h4' ?
          <StyledH4 size={props.size}>{props.children}</StyledH4> :
        props.is === 'h5' ?
          <StyledH5 size={props.size}>{props.children}</StyledH5> :
        props.is === 'h6' ?
          <StyledH6 size={props.size}>{props.children}</StyledH6> : <StyledH2 size={props.size}>{props.children}</StyledH2>
      }
    </React.Fragment>
  )
}

export const Paragraph = props => {
  return(
    <StyledParagraph>{props.children}</StyledParagraph>
  )
}

export const Text = props => {
  return(
    <StyledText className={props.className}>{props.children}</StyledText>
  )
}