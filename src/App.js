import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

import User from './pages/Administrator/User/User'
import UserCreate from './pages/Administrator/User/UserCreate'
import UserEdit from './pages/Administrator/User/UserEdit'

export default class App extends Component {
  render() {
    return (
      <Router>
        <div className="app">
          <header className="app-header">
            <ul>
              <li><Link to="/">List</Link></li>
              <li><Link to="/create">Create</Link></li>
            </ul>
          </header>
          <main className="app-main" style={{padding: `45px 0`}}>
            <Route path="/" exact component={User} />
            <Route path="/create" component={UserCreate} />
            <Route path="/edit/:id" component={UserEdit} />
          </main>
          <footer className="app-footer">
            <div className="container"></div>
          </footer>
        </div>
      </Router>
    );
  }
}
